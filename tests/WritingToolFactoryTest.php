<?php

use PenTest\PenTypes;
use PenTest\Pen\Pen;
use PenTest\Pen\Pencil;
use PenTest\WritingToolFactory;

class WritingToolFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatePen()
    {
        $factory = new WritingToolFactory();
        $pen = $factory->create(PenTypes::PEN);

        $this->assertTrue($pen instanceof Pen);
    }

    public function testCreatePencil()
    {
        $factory = new WritingToolFactory();
        $pen = $factory->create(PenTypes::PENCIL);

        $this->assertTrue($pen instanceof Pencil);
    }
}
