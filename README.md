Pen Factory Demo
===


Installing
---

Clone the repository and run following command:

    composer install

Testing
---

Run following command and all test should be successful:

    phpunit
